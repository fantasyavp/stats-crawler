package fantasyavp.data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table( name = "player" )
public class Player
{
    @Id
    @Column( name = "playerid", unique = true)
    private Integer playerId;

    @Column( name = "name" )
    private String name;

    @Column( name = "rank" )
    private Integer rank;

    @Column( name = "points" )
    private Integer points;

    public Player() {}

    public Player( Integer playerId, String name, Integer rank, Integer points )
    {
        setPlayerId( playerId );
        setName( name );
        setRank( rank );
        setPoints( points );
    }

    public void setPlayerId( Integer playerId )
    {
        this.playerId = playerId;
    }
    public Integer getPlayerId()
    {
        return playerId;
    }

    public void setName( String name )
    {
        this.name = name;
    }
    public String getName()
    {
        return name;
    }

    public void setRank( Integer rank )
    {
        this.rank = rank;
    }
    public Integer getRank()
    {
        return rank;
    }

    public void setPoints( Integer points )
    {
        this.points = points;
    }
    public Integer getPoints()
    {
        return points;
    }
}