package fantasyavp.data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table( name = "match" )
public class Match
{
    @Id
    @Column( name = "uuid" )
    private UUID uuid;

    @Column( name = "game" )
    private Integer game;

    @Column( name = "matchid" )
    private Integer matchId;

    @Column( name = "playerid" )
    private Integer playerId;

    @Column( name = "attack_attacks" )
    private Integer attackAttacks;

    @Column( name = "attack_kills" )
    private Integer attackKills;

    @Column( name = "attack_errors" )
    private Integer attackErrors;

    @Column( name = "attack_hittingpct" )
    private Double attackHittingPct;

    @Column( name = "serve_aces" )
    private Integer serveAces;

    @Column( name = "serve_errors" )
    private Integer serveErrors;

    @Column( name = "defense_blocks" )
    private Integer defenseBlocks;

    @Column( name = "defense_digs" )
    private Integer defenseDigs;

    public Match() {}

    public Match(
                Integer game,
                Integer matchId,
                Integer playerId,
                Integer attackAttacks,
                Integer attackKills,
                Integer attackErrors,
                Double attackHittingPct,
                Integer serveAces,
                Integer serveErrors,
                Integer defenseBlocks,
                Integer defenseDigs )
    {

        setId();
        setGame( game );
        setMatchId( matchId );
        setPlayerId( playerId );

        setAttackAttacks( attackAttacks );
        setAttackKills( attackKills );
        setAttackErrors( attackErrors );
        setAttackHittingPct( attackHittingPct );

        setServeAces( serveAces );
        setServeErrors( serveErrors );

        setDefenseBlocks( defenseBlocks );
        setDefenseDigs( defenseDigs );
    }

    public void setId()
    {
        this.uuid = UUID.randomUUID();
    }
    public UUID getId()
    {
        return uuid;
    }

    public void setGame( Integer game )
    {
        this.game = game;
    }
    public Integer getGame()
    {
        return game;
    }

    public void setMatchId( Integer matchId )
    {
        this.matchId = matchId;
    }
    public Integer getMatchId()
    {
        return matchId;
    }

    public void setPlayerId( Integer playerId )
    {
        this.playerId = playerId;
    }
    public Integer getPlayerId()
    {
        return playerId;
    }

    //

    public void setAttackAttacks( Integer attackAttacks )
    {
        this.attackAttacks = attackAttacks;
    }
    public Integer getAttackAttacks()
    {
        return attackAttacks;
    }

    public void setAttackKills( Integer attackKills )
    {
        this.attackKills = attackKills;
    }
    public Integer getAttackKills()
    {
        return attackKills;
    }

    public void setAttackErrors( Integer attackErrors )
    {
        this.attackErrors = attackErrors;
    }
    public Integer getAttackErrors()
    {
        return attackErrors;
    }

    public void setAttackHittingPct( Double attackHittingPct )
    {
        this.attackHittingPct = attackHittingPct;
    }
    public Double getAttackHittingPct()
    {
        return attackHittingPct;
    }

    public void setServeAces( Integer serveAces )
    {
        this.serveAces = serveAces;
    }
    public Integer getServeAces()
    {
        return serveAces;
    }

    public void setServeErrors( Integer serveErrors )
    {
        this.serveErrors = serveErrors;
    }
    public Integer getServeErrors()
    {
        return serveErrors;
    }

    //Defense
    public void setDefenseBlocks( Integer defenseBlocks )
    {
        this.defenseBlocks = defenseBlocks;
    }
    public Integer getDefenseBlocks()
    {
        return defenseBlocks;
    }

    public void setDefenseDigs( Integer defenseDigs )
    {
        this.defenseDigs = defenseDigs;
    }
    public Integer getDefenseDigs()
    {
        return defenseDigs;
    }
}