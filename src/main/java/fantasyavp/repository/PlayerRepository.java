package fantasyavp.repository;

import fantasyavp.data.Player;

public interface PlayerRepository {
    Player getByPlayerId( Integer playerId );
    Player getByPlayerName( String name );
    void create( Player player );
    void update( Player player );
}