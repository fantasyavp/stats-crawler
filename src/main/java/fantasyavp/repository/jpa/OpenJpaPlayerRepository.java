package fantasyavp.repository.jpa;

import fantasyavp.data.Player;
import fantasyavp.repository.PlayerRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

import static javax.persistence.Persistence.createEntityManagerFactory;

public class OpenJpaPlayerRepository implements PlayerRepository {
    private EntityManager manager;

    public OpenJpaPlayerRepository(EntityManager manager) {
        this.manager = manager;
    }
    public OpenJpaPlayerRepository() {
        manager = createEntityManagerFactory("simplePU").createEntityManager();
    }

    @Override
    public Player getByPlayerId( Integer playerId )
    {
        return manager.find(Player.class, playerId);
    }

    @Override
    public Player getByPlayerName( String name )
    {
        return (Player) manager.createQuery("SELECT p FROM Player p WHERE p.name =: name")
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public void create( Player player )
    {
        manager.getTransaction().begin();
        manager.persist(player);
        manager.getTransaction().commit();
    }

    @Override
    public void update( Player player )
    {
        manager.getTransaction().begin();
        manager.merge(player);
        manager.getTransaction().commit();
    }
}