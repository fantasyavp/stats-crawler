package fantasyavp.repository.jpa;

import fantasyavp.data.Match;
import fantasyavp.repository.MatchRepository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

import static javax.persistence.Persistence.createEntityManagerFactory;

public class OpenJpaMatchRepository implements MatchRepository {
    private EntityManager manager;

    public OpenJpaMatchRepository(EntityManager manager) {
        this.manager = manager;
    }
    public OpenJpaMatchRepository() {
        manager = createEntityManagerFactory("simplePU").createEntityManager();
    }

    @Override
    public Match getByMatchId( Integer matchId ) {
        return manager.find(Match.class, matchId);
    }

    @Override
    public void create( Match match ) {
        manager.getTransaction().begin();
        manager.persist(match);
        manager.getTransaction().commit();
    }

    @Override
    public void update( Match match ) {
        manager.getTransaction().begin();
        manager.merge(match);
        manager.getTransaction().commit();
    }
}