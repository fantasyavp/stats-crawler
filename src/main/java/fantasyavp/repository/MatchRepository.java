package fantasyavp.repository;

import fantasyavp.data.Match;

public interface MatchRepository {
    Match getByMatchId( Integer matchId );
    void create( Match match );
    void update( Match match );
}
